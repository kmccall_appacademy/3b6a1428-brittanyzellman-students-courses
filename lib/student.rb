require 'byebug'
class Student

  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  # getters for @first_name and @last_name
  def first_name
    @first_name
  end

  def last_name
    @last_name
  end

  def name
    "#{@first_name} #{@last_name}"
  end

  def courses
    @courses
  end

  def enroll(course)
    # debugger
    courses.each do |c|
      if c.conflicts_with?(course)
        conflict_error
      end
    end
    @courses << course if !@courses.include?(course)
    course.students << self if !course.students.include?(self)
    @courses
  end

  def course_load
    c_load = Hash.new(0)
    courses.each do |course|
      c_load[course.department] += course.credits
    end
    c_load
  end

  def conflict_error
    raise 'ERROR: courses conflict'
  end

end
